sap.ui.define(['sap/ui/core/mvc/Controller', 'sap/ui/model/json/JSONModel'],
	function(Controller, JSONModel) {
		"use strict";

		return Controller.extend("auditcreate.blocks.create.Create", {

			onInit: function() {
				//var myDate = new Date();
				
				var data = {
						//startDate : myDate.getDate(),
						//endDate : myDate.getDate() + 2
						
						startDate : new Date(), 
						endDate: new Date() 
				};

				//myDate = this.oFormatYyyymmdd.format(sta);
				var oModel = new sap.ui.model.json.JSONModel();
				oModel.setData(data);
				sap.ui.getCore().setModel(oModel);

			},
			
			onPress: function(oEvent) {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("productInformation");
			}
		});
	});