sap.ui.define(['sap/ui/core/mvc/Controller', 'sap/ui/model/json/JSONModel'],
	function(Controller, JSONModel) {
		"use strict";

		return Controller.extend("auditcreate.blocks.product.Product", {

			onInit: function() {
				var data = {
				};

				var oModel = new sap.ui.model.json.JSONModel();
				oModel.setData(data);
				sap.ui.getCore().setModel(oModel);

			}
		});
	});